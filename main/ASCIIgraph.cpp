#include <iostream>
#include <conio2.h>
#include "ASCIIgraph.h"

using namespace std;

// konstruktor klasy TASCII
TASCII::TASCII()
   {
   Color = LIGHTGRAY; // domyslny kolor
   Style = asStyle2; // domyslny styl (podwojna linia)
   }

/* ========================= FUNKCJE RYSUJACE ================================

 Ponizsze funkcje sluza do rysowania poszczegolnych elementow ramek czyli linii
 poziomej, pionowej, rogow i wszelkich skrzyzowan linii.
 Kazda z funkcji dziala w podobny sposob:
 - ustawiany jest kolor (taki jaki jest zdefiniowany w zmiennej Color 
   klasy TASCII)
 - kursor jest przenoszony w odpowiednie wspolrzedne na ekranie (podane jako 
   parametry funkcji)
 - wyswietlany zostaje odpowiedni znak na ekranie w zaleznosci od typu ramki
 - w przypadku linni rysowanie wykonywane jest w petli az do narysowania calej
   linii

*/
int TASCII::LineH(int x1, int y, int x2)
   {
   textcolor(Color);
   gotoxy(x1, y);
   for (int i=x1; i<=x2; i++)
      {
      putchar(Graph[Style][lnHorisontal]);
      }
   }

int TASCII::LineV(int x, int y1, int y2)
   {
   textcolor(Color);
   for (int i=y1; i<=y2; i++)
      {
      gotoxy(x, i);
      putchar(Graph[Style][lnVertical]);
      }
   }

int TASCII::Corner(int x, int y, int CornerType)
   {
   textcolor(Color);
   gotoxy(x, y);
   putchar(Graph[Style][CornerType]);
   }
