#ifndef __ramka_H__
#define __ramka_H__

// typy ramek
const int trLinia1 = 0;
const int trLinia2 = 1;

class TRamka
   {
   public:
      int x1, y1, x2, y2;
      int typ;
      int kolor;
      TRamka();
      TRamka(int x1,int y1,int x2,int y2, int typ, int kolor);
      int Rysuj();
   };

#endif
