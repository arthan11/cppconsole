#include <iostream>
#include <conio2.h>
#include"ramka.h"

using namespace std;

TRamka::TRamka()
   {
   x1    = 1;
   y1    = 1;
   x2    = 79;
   y2    = 25;
   typ   = trLinia1;
   kolor = LIGHTGRAY;
   }

TRamka::TRamka(int x1,int y1,int x2,int y2, int typ, int kolor)
   {
   TRamka::x1 = x1;
   TRamka::y1 = y1;
   TRamka::x2 = x2;
   TRamka::y2 = y2;
   TRamka::typ = typ;
   TRamka::kolor = kolor;
   }

const char ZN_TOP_LEFT  = 0;
const char ZN_TOP_RIGHT = 1;
const char ZN_BOTTOM_LEFT = 2;
const char ZN_BOTTOM_RIGHT = 3;
const char ZN_HORISONTAL = 4;
const char ZN_VERTICAL = 5;

int TRamka::Rysuj()
   {
   textcolor(kolor);
   char znaki[2][6] = {218,191,192,217,196,179,
                       201,187,200,188,205,186};
   // rogi
   gotoxy(x1, y1); putchar(znaki[typ][ZN_TOP_LEFT]);
   gotoxy(x2, y1); putchar(znaki[typ][ZN_TOP_RIGHT]);
   gotoxy(x1, y2); putchar(znaki[typ][ZN_BOTTOM_LEFT]);
   gotoxy(x2, y2); putchar(znaki[typ][ZN_BOTTOM_RIGHT]);
   // linie poziome
   for (int i=x1+1; i<x2; i++)
      {
      gotoxy(i, y1); putchar(znaki[typ][ZN_HORISONTAL]);
      gotoxy(i, y2); putchar(znaki[typ][ZN_HORISONTAL]);
      }
   // linie pionowe
   for (int i=y1+1; i<y2; i++)
      {
      gotoxy(x1, i); putchar(znaki[typ][ZN_VERTICAL]);
      gotoxy(x2, i); putchar(znaki[typ][ZN_VERTICAL]);
      }
   // ustawia kursor wewnatrz ramki
   gotoxy(x1+1, y1+1);
   }

