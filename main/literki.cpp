#include <iostream>
#include <conio2.h>
#include "literki.h"
using namespace std;

const char WYS_LITER = 7;
const char ILOSC_LITER = 27;

// tablica ze znakami wyswietlanymi na ekranie przez funkcje
const string LITERY[ILOSC_LITER][WYS_LITER] =
     {"       ",
      "       ",
      "       ",
      "       ",
      "       ",
      "       ",
      "       ",

      "  XX   ",
      " XXXX  ",
      "XX  XX ",
      "XX  XX ",
      "XXXXXX ",
      "XX  XX ",
      "XX  XX ",

      "XXXXX  ",
      "XX  XX ",
      "XX  XX ",
      "XXXXX  ",
      "XX  XX ",
      "XX  XX ",
      "XXXXX  ",

      " XXXX  ",
      "XX  XX ",
      "XX     ",
      "XX     ",
      "XX     ",
      "XX  XX ",
      " XXXX  ",

      "XXXX   ",
      "XX XX  ",
      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      "XX XX  ",
      "XXXX   ",

      "XXXXXX ",
      "XX     ",
      "XX     ",
      "XXXX   ",
      "XX     ",
      "XX     ",
      "XXXXXX ",

      "XXXXXX ",
      "XX     ",
      "XX     ",
      "XXXX   ",
      "XX     ",
      "XX     ",
      "XX     ",

      " XXXX  ",
      "XX  XX ",
      "XX     ",
      "XX     ",
      "XX XXX ",
      "XX  XX ",
      " XXXX  ",

      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      "XXXXXX ",
      "XX  XX ",
      "XX  XX ",
      "XX  XX ",

      " XXXX  ",
      "  XX   ",
      "  XX   ",
      "  XX   ",
      "  XX   ",
      "  XX   ",
      " XXXX  ",

      "    XX ",
      "    XX ",
      "    XX ",
      "    XX ",
      "    XX ",
      "XX  XX ",
      " XXXX  ",

      "XX  XX ",
      "XX  XX ",
      "XX XX  ",
      "XXXX   ",
      "XX XX  ",
      "XX  XX ",
      "XX  XX ",

      "XX     ",
      "XX     ",
      "XX     ",
      "XX     ",
      "XX     ",
      "XX     ",
      "XXXXXX ",

      "XX   XX",
      "XXX XXX",
      "XXXXXXX",
      "XX X XX",
      "XX   XX",
      "XX   XX",
      "XX   XX",

      "XX   XX",
      "XXX  XX",
      "XXXX XX",
      "XX XXXX",
      "XX  XXX",
      "XX   XX",
      "XX   XX",

      " XXXX  ",
      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      " XXXX  ",

      "XXXXX  ",
      "XX  XX ",
      "XX  XX ",
      "XXXXX  ",
      "XX     ",
      "XX     ",
      "XX     ",

      " XXXX  ",
      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      "XX XXX ",
      " XXXX  ",
      "    XX ",

      "XXXXX  ",
      "XX  XX ",
      "XX  XX ",
      "XXXXX  ",
      "XX XX  ",
      "XX  XX ",
      "XX  XX ",

      " XXXX  ",
      "XX  XX ",
      " XX    ",
      "  XX   ",
      "   XX  ",
      "XX  XX ",
      " XXXX  ",

      "XXXXXX ",
      "  XX   ",
      "  XX   ",
      "  XX   ",
      "  XX   ",
      "  XX   ",
      "  XX   ",

      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      " XXXX  ",

      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      " XXXX  ",
      "  XX   ",

      "XX   XX",
      "XX   XX",
      "XX   XX",
      "XX X XX",
      "XXXXXXX",
      "XXX XXX",
      " XX XX ",

      "XX  XX ",
      "XX  XX ",
      " XX X  ",
      "  XX   ",
      " X XX  ",
      "XX  XX ",
      "XX  XX ",

      "XX  XX ",
      "XX  XX ",
      "XX  XX ",
      " XXXX  ",
      "  XX   ",
      "  XX   ",
      "  XX   ",

      "XXXXXX ",
      "    XX ",
      "   XX  ",
      "  XX   ",
      " XX    ",
      "XX     ",
      "XXXXXX "};

/* ponizsza funkcja wykozystywana jest przez funkcje WriteBIG do zmieniania
   znakow X i spacji z tablicy zawierajacej litery na znaki podane w parametrach 
   Chr1 i Chr2 */

string SetStyle(string Text, char Chr1, char Chr2)
  {
  string Result = "";
  // w petli po wszystkich znakach w zmiennej Text
  for (int i=0; i <= Text.length() -1; i++)
    {
    if (Text[i] == ' ') // jesli znak to spacja...
      Result += Chr2;   // to zapisuje do wyniku funkcji znak ze zmiennej Chr2
    else                // w przeciwnym wypadku...
      Result += Chr1;   // zapisuje do wyniku funkcji znak ze zmiennej Chr1
    }
  return Result;
  }

// ======================= FUNKCJA WriteBIG ====================================
void WriteBIG(int X, int Y, string Text, char Chr1, char Chr2)
  {
  // jezeli jako wspolzedne podany jest parametr POS_CENTER to wyswietla na
  // srodku ekranu
  if (X == POS_CENTER)
    {
    X = (81 - Text.length()*7) / 2; // 
    }
  if (Y == POS_CENTER)
    {
    Y = (27 - WYS_LITER)/2;
    }

  // rysuje tylko wtedy gdy napis miesci sie na ekranie
  if ((Text.length() * 7 + X -2 <= 80) and (Y+7 <= 25))
    {
    // litery sa rysowane w petli - poziomo, wierszami
    // petla przez wszystkie wiersze
    for (int wier=0; wier<WYS_LITER; wier++)
      {
      gotoxy(X, Y + wier); // przejscie do odpowiedniego wiersza
      // petla przez wszystkie litery
      for (int litera=0; litera<Text.length(); litera++)
        {
        // zamienianie kodu ASCII litery na jej pozycje w tablicy LITERY 
        int idx = Text[litera] - 64;
        // jezeli znak nie ma odpowiednika w tablicy 
        if ((idx > ILOSC_LITER-1) or (idx < 0))
          {
          idx = 0; //to wybiera pierwszy element w tablicy (czyli spacje)
          }
        // wyswietla pojedynczy wiersz znaku na ekranie zamieniajac najpierw
        // znaki X i spacje na znaki ze zmiennych Chr1 i Chr2 uzywajac do tego
        // celu funckji SetStyle
        cout << SetStyle(LITERY[idx][wier], Chr1, Chr2);
        }
      }
    }
  }
