#include <iostream>
#include <conio2.h>
#include "menu.h"

using namespace std;

TElement::TElement()
   {
   funct = NULL;
   }

int TMenu::Init()
   {
   int max = 0;
   for (int i=0; i<MAX_ELEMENTS; i++)
      {
      if (max < Elements[i].Name.length())
          max = Elements[i].Name.length();
      }
   if (max < Title.length())
      Width = Title.length() + 2;
   else
      Width = max + 2;

   PosX = (80 - Width) / 2;
   PosY = (20 - Count*3) / 2;
   }

int TMenu::Select(int Id)
   {
   int OldId = Selected;
   Selected = Id;
   DrawElement(OldId);
   DrawElement(Id);
   }

int TMenu::SelectNext()
   {
   if (Selected+1 < Count)
      Select(Selected+1);
   else
      Select(0);
   }

int TMenu::SelectPrev()
   {
   if (Selected > 0)
      Select(Selected-1);
   else
      Select(Count-1);
   }

int TMenu::Add(string Name, int (*fnc)())
   {
   if (Count < MAX_ELEMENTS)
      {
      Count++;
      Elements[Count-1].Name = Name;
      Elements[Count-1].funct = fnc;
      }
   }

int TMenu::Clear()
   {
   Count = 0;
   }

TMenu::TMenu()
   {
   Count = 0;
   PosX = 1;
   PosY = 1;
   Title = "MENU";
   Selected = 0;
   ClrScr = true;
   Background = BLACK;
   SelectedColor = WHITE;
   SelectedBackground = RED;
   ItemColor = BLUE;
   ItemBackground = CYAN;
   TitleColor = BLUE;
   TitleBackground = LIGHTGRAY;
   FrameColor = GREEN;
   FrameBackground = BLACK;
   FrameStyle = asStyle2;
   }

int TMenu::DrawElement(int Id)
   {
   if (Id == Selected)
      {
      textcolor(SelectedColor);
      textbackground(SelectedBackground);
      }
   else
      {
      textcolor(ItemColor);
      textbackground(ItemBackground);
      }

   int tempY = PosY+(Id+1)*3-1+4;
   int l = (Width - Elements[Id].Name.length()) / 2;

   gotoxy(PosX+1, tempY-1);
   cout.width(Width);
   cout << "";
   gotoxy(PosX+1, tempY+1);
   cout.width(Width);
   cout << "";

   gotoxy(PosX+1, tempY);
   cout.width(l);
   cout << "";
   cout.width(Width - l);
   cout << left << Elements[Id].Name;
   }

int TMenu::AfterDraw()
   {
   }

int TMenu::Draw()
   {
   Init();
   cout.fill(' ');

// ekran
  _setcursortype(_NORMALCURSOR);
   if (ClrScr)
      {
      textbackground(Background);
      clrscr();
      }

// ramka
   ASCII.Color = FrameColor;
   ASCII.Style = FrameStyle;
   textbackground(FrameBackground);
   ASCII.Corner(PosX, PosY, crTopLeft);
   ASCII.Corner(PosX+Width+1, PosY, crTopRight);
   ASCII.Corner(PosX, PosY+4, crRightVertical);
   ASCII.Corner(PosX+Width+1, PosY+4, crLeftVertical);
   ASCII.Corner(PosX, PosY+Count*3+5, crBottomLeft);
   ASCII.Corner(PosX+Width+1, PosY+Count*3+5, crBottomRight);
   ASCII.LineH(PosX+1, PosY, PosX+Width);
   ASCII.LineH(PosX+1, PosY+4, PosX+Width);
   ASCII.LineV(PosX, PosY+1, PosY+3);
   ASCII.LineV(PosX+Width+1, PosY+1, PosY+3);
   ASCII.LineV(PosX, PosY+5, PosY+Count*3+4);
   ASCII.LineV(PosX+Width+1, PosY+5, PosY+Count*3+4);
   ASCII.LineH(PosX+1, PosY+Count*3+5, PosX+Width);

// tytul
   textcolor(TitleColor);
   textbackground(TitleBackground);
   int l = (Width - Title.length()) / 2;

   gotoxy(PosX+1 ,PosY+1);
   cout.width(Width);
   cout << "";
   gotoxy(PosX+1 ,PosY+3);
   cout.width(Width);
   cout << "";
   gotoxy(PosX+1 ,PosY+2);
   cout.width(l);
   cout << "";
   cout.width(Width - l);
   cout << left << Title;

// elementy
   for (int i=0; i<Count; i++)
      {
      DrawElement(i);
      }

   AfterDraw();
   }

int TMenu::Go()
  {
   int k;
   while(k!=27)
      {
      k = getch(); // pobiera nr klawisza
      switch (k)
         {
         case 72:            // w gore
            SelectPrev(); break;
         case 80:            // w dol
            SelectNext(); break;
         case 13:
            if (Elements[Selected].funct != NULL)
               {
               Elements[Selected].funct();
               Draw();
               break;
               }
         }
      }
  }
