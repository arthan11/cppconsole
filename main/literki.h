/****************************************************************
  plik pomocny przy rysowaniu duzych liter

  Funkcja ponizej sluzy do rysowania duzych liter. Gorny lewy rog miejsca,
  w ktorym maja byc rysowane litery podawany jest w parametrach X,Y.
  Tekst do wyswietlenia podaje sie przez zmienna Text. Przez parametr Chr1
  podaje sie znak z ktorego funkcja buduje litery, a Chr2 znak ktorym wypelniane
  jest tlo liter. Jako pozycje liter na ekranie mozna podac POS_CENTER by
  napis automatycznie wyswietlal sie na srodku ekranu.
  (Jak narazie funkcja wyswietla jedynie duze litery, bez polskich i bez cyfr)

****************************************************************/
#ifndef __literki_H__
#define __literki_H__

#include <iostream>
using namespace std;

const int POS_CENTER = 255;

void WriteBIG(int X, int Y, string Text, char Chr1, char Chr2);

#endif
