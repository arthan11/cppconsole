/****************************************************************
  plik uzywany do obslugi menu
****************************************************************/
#ifndef __menu_H__
#define __menu_H__
#include <iostream>
#include "ASCIIgraph.h" // kozysta z ASCIIgraph do rysowania ramek
using namespace std;

const int MAX_ELEMENTS = 6; // maksymalna ilosc opcji menu

// klasa pojedynczej opcji w menu
class TElement
   {
   public:
     string Name;      // nazwa opcji
     int (*funct)();    // procedurka opcji menu
     TElement();
   };

class TMenu
   {
   private:
      TASCII ASCII;        // klasa do rysowania ramek
      char PosX, PosY;     // pozycja menu (domyslnie na srodku)
      int Width;           // szerokosc pozycji w menu
      int Init();
      char Selected;       // index wybranego elementu
      int Select(int Id);
      int DrawElement(int Id);
   public:
      bool ClrScr;         // czy czyscic ekran poza obszarem menu
      char Background;        // kolor tla programu w czasie pokazywania menu
      char SelectedColor;     // kolor zaznaczanoge elementu
      char SelectedBackground;     // kolor tla zaznaczonego elementu
      char ItemColor;     // kolor elementu
      char ItemBackground;     // kolor tla elementu
      char TitleColor;     // kolor tytulu
      char TitleBackground;     // kolor tla tytulu
      char FrameColor;     // kolor obramowania
      char FrameBackground; // kolor tla obramowania
      char FrameStyle;     // styl obramowania (pojedyncze czy podwojne)
      char Count;          // ilosc elementow
      string Title;        // tytul
      TElement Elements[MAX_ELEMENTS]; // tablica elementow
      TMenu();
      int Add(string Name, int (*fnc)()); // dodawanie elementow
      int Draw();    // rysowanie menu
      virtual int AfterDraw(); // funkcja wykonywana przy rysowaniu (do nadpisania)
      int Clear(); // usuwanie elementow menu
      int SelectNext(); // wybranie nastepnego elementu
      int SelectPrev(); // wybranie poptrzedniego elementu
      int Go(); // petla menu
   };

#endif
