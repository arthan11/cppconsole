/****************************************************************
plik pomocny przy rysowaniu linni i ramek w trybie tekstowym

****************************************************************/
#ifndef __ASCIIgraph_H__
#define __ASCIIgraph_H__

// tablica zawierajaca znaki wykorzystywane przy rysowaniu ramek
const char Graph[2][11] = {218,191,192,217,196,179,193,194,180,195,197,
                           201,187,200,188,205,186,202,203,185,204,206};

// ponizej stale wykorzystywane do nazywania elementow i stylow ramek

// corners
const char crTopLeft          = 0;
const char crTopRight         = 1;
const char crBottomLeft       = 2;
const char crBottomRight      = 3;
// lines
const char lnHorisontal       = 4;
const char lnVertical         = 5;
// corners++
const char crTopHorisontal    = 6;
const char crBottomHorisontal = 7;
const char crLeftVertical     = 8;
const char crRightVertical    = 9;
const char crCross            = 10;

// ASCII styles
const char asStyle1 = 0;
const char asStyle2 = 1;

// corners type
const char ctNormal      = 0;
const char ctH           = 1;
const char ctV           = 2;

// klasa sluzaca do rysowania
class TASCII
   {
   public:
     char Color;   // kolor rysowania
     char Style;    // podwojne czy pojedyncze linie
     int LineV(int x, int y1, int y2); // rysuje linie pionowa
     int LineH(int x1, int y, int x2); // rysuje linie pozioma
     int Corner(int x, int y, int CornerType); // rysuje rogi ramki
     TASCII();
   };

#endif
