#include <iostream>
#include <conio2.h>
#include "..\..\main\menu.h"
#include "..\..\main\literki.h"
#include "sudoku.h"

using namespace std;

const string VER         = "1.01";
const string AUTHOR      = "Krzysztof Lemka";
const string TITLE       = "SUDOKU";

int fnStart()
   {
   int x;
   // czyszczenie tla
   textbackground(BLACK);
   clrscr();
   // tytul
   textbackground(BLUE);
   textcolor(YELLOW);
   WriteBIG(POS_CENTER, POS_CENTER, TITLE, 219, ' ');
   // wersja
   x = (77 - VER.length()) / 2;
   gotoxy(x , 19);
   textbackground(BLACK);
   textcolor(LIGHTCYAN);
   cout << "ver " << VER;
   // autor
   x = 75 - AUTHOR.length();
   gotoxy(x, 23);
   textbackground(BLACK);
   textcolor(LIGHTGRAY);
   cout << "by " << AUTHOR;
   getch();
   }

int fnEnd()
  {
  exit(0);
  }

int fnMenu()
   {
   TMenu Menu;
   Menu.TitleColor = YELLOW;
   Menu.TitleBackground = BLUE;
   Menu.ItemColor = WHITE;
   Menu.SelectedColor = WHITE;
   Menu.ItemBackground = BLUE;
   Menu.SelectedBackground = LIGHTBLUE;
   Menu.FrameColor= LIGHTBLUE;
   Menu.FrameBackground= BLUE;

   // Add MAX 6
   Menu.Add("    NOWA GRA    ", NewGame);
   Menu.Add("     KONIEC     ", fnEnd);
   Menu.Draw();

   Menu.Go();
   }

int main()
   {
   fnStart();
   fnMenu();
   }
