#ifndef __sudoku_H__
#define __sudoku_H__

struct TSelected
  {
  int X;
  int Y;
  };

struct TField
  {
  int Number;
  bool Possibilities[9];
  };

class TGame
  {
  private:
    TField Board[9][9];
  public:
    int X, Y;                         // przesuniecie planszy
    TGame();
    TSelected Selected;               // zaznaczony element
    int DrawBoard();                  // rysowanie planszy
    int DrawAllNumbers();             // rysowanie cyfr & kursora
    int DrawNumber(int AX, int AY);   // rysowanie wybranej cyfry
    int DrawCursor();                 // rysowanie kursora
    int SetNumber(int AX, int AY, int Number); // wprowadzenie cyfry
    int Clear();                      // czyszczenie cyfr
    int Generate();                   // generowanie cyfr
    int CountPossibilities(int AX, int AY);         // oblicza jakie liczby moga byc w polach
  };

int NewGame();

#endif
