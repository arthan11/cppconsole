#include <iostream>
#include <conio2.h>

using namespace std;

struct TSelected
  {
  int X;
  int Y;
  };

struct TField
  {
  int Number;
  bool Possibilities[9];
  };

class TGame
  {
  private:
    TField Board[9][9];
  public:
    int X, Y;                         // przesuniecie planszy
    TGame();
    TSelected Selected;               // zaznaczony element
    int DrawBoard();                  // rysowanie planszy
    int DrawAllNumbers();             // rysowanie cyfr & kursora
    int DrawNumber(int AX, int AY);   // rysowanie wybranej cyfry
    int DrawCursor();                 // rysowanie kursora
    int SetNumber(int AX, int AY, int Number); // wprowadzenie cyfry
    int Clear();                      // czyszczenie cyfr
    int Generate();                   // generowanie cyfr
    int CountPossibilities(int AX, int AY);         // oblicza jakie liczby moga byc w polach
  };

int DrawFirstLine(int X, int Y)
  {
  gotoxy(X,Y);
  cout << char(201);
  for(int i=0;i<9;i++)
    {
    cout << char(205) << char(205) << char(205);
    if(i<8)
      cout << char(203);
    }
  cout << char(187);
  }

int DrawStdLine1(int X, int Y)
  {
  gotoxy(X,Y);
  cout << char(186);
  for(int i=1;i<=9;i++)
    {
    cout << "   ";
    if(i%3)
      cout << char(179);
    else
      cout << char(186);
    }
  }

int DrawStdLine2(int X, int Y)
  {
  gotoxy(X,Y);
  cout << char(186);
  for(int i=1;i<=9;i++)
    {
    cout << char(196) << char(196) << char(196) ;
    if(i%3)
      cout << char(197);
    else
      cout << char(186);
    }
  }

int DrawStdLine3(int X, int Y)
  {
  gotoxy(X,Y);
  cout << char(186);
  for(int i=0;i<9;i++)
    {
    cout << char(205) << char(205) << char(205);
    if(i<8)
      cout << char(206);
    }
  cout << char(186);
  }

int DrawLastLine(int X, int Y)
  {
  gotoxy(X,Y);
  cout << char(200);
  for(int i=0;i<9;i++)
    {
    cout << char(205) << char(205) << char(205);
    if(i<8)
      cout << char(202);
    }
  cout << char(188);
  }

int TGame::DrawBoard()
  {
  int local_X = X;
  int local_Y = Y;
  DrawFirstLine(local_X,local_Y++);
  for(int i=0;i<=2;i++)
    {
    DrawStdLine1(local_X,local_Y++);
    DrawStdLine2(local_X,local_Y++);
    DrawStdLine1(local_X,local_Y++);
    DrawStdLine2(local_X,local_Y++);
    DrawStdLine1(local_X,local_Y++);
    if (i<2)
      DrawStdLine3(local_X,local_Y++);
    }
  DrawLastLine(local_X,local_Y++);
  }

int TGame::DrawAllNumbers()
  {
  for(int i=0;i<9;i++)
    for(int j=0;j<9;j++)
      {
      DrawNumber(i, j);
      }
  }

int TGame::DrawNumber(int AX, int AY)
  {
  gotoxy(X+AX*4+2,Y+AY*2+1);
  if (Board[AX][AY].Number!=0)
    cout << Board[AX][AY].Number;
  else
    cout << ' ';
  DrawCursor();
  }

int TGame::DrawCursor()
  {
  CountPossibilities(Selected.X, Selected.Y);

  gotoxy(X + ( Selected.X * 4 +2 ), Y + Selected.Y * 2 +1 );
  _setcursortype(_SOLIDCURSOR);
  }

int TGame::SetNumber(int AX, int AY, int Number)
  {
  if ((Board[Selected.X][Selected.Y].Possibilities[Number-1] == true) or Number==0)
    {
    Board[AX][AY].Number = Number;
    DrawNumber(AX, AY);
    }
  }

int TGame::Generate()
  {
// tylko przyklad - DO POPRAWY
  srand(time(NULL));
  for(int i=0;i<9;i++)
    for(int j=0;j<9;j++)
      {
      int A = rand()%10;
      if (A > 9) A = 0;
      Board[i][j].Number = 0;//A;
      }
  }

int TGame::CountPossibilities(int AX, int AY)
  {
  // wszystkie mozliwe
  for (int i=0;i<9;i++)
      Board[AX][AY].Possibilities[i] = true;

  for (int i=0;i<9;i++)
    {
    // usun z kolumny
      if (i != AY)
        if (Board[AX][i].Number > 0)
          Board[AX][AY].Possibilities[Board[AX][i].Number-1] = false;
    // usun z wiersza
      if (i != AX)
        if (Board[i][AY].Number > 0)
          Board[AX][AY].Possibilities[Board[i][AY].Number-1] = false;
    }

  // usun z 3x3
  for (int i = ( int(AX) /3 )*3 ; i < (int(AX )/3) *3 +3;i++)
    for (int j = ( int(AY) /3 )*3 ; j < (int(AY )/3) *3 +3;j++)
      {
      if ((i != AX) or (j != AY))
        if (Board[i][j].Number > 0)
          Board[AX][AY].Possibilities[Board[i][j].Number-1] = false;
      }

  gotoxy(1,1);
  cout.width(20);
  cout << ' ';
  gotoxy(1,1);
  for (int i=0;i<9;i++)
    if (Board[AX][AY].Possibilities[i] == true)
      cout << i+1 << ' ';

  }

TGame::TGame()
  {
  Clear();
  Selected.X = 0;
  Selected.Y = 0;
  X = 1;
  Y = 1;
  }

int TGame::Clear()
  {
  for(int i=0;i<9;i++)
    for(int j=0;j<9;j++)
      Board[i][j].Number = 0;
  }

int NewGame()
  {
  textbackground(BLACK);
  clrscr();

  TGame Game;

  Game.Generate();
  Game.X = 22;
  Game.Y = 4;
  Game.DrawBoard();
  Game.DrawAllNumbers();

  int Key;
  do
    {
    Key = getch();
    // poruszanie strzalkami
    if (Key==224)
      {
      switch(getch())
        {
        case 75: if (Game.Selected.X > 0) Game.Selected.X--; break;
        case 77: if (Game.Selected.X < 8) Game.Selected.X++; break;
        case 72: if (Game.Selected.Y > 0) Game.Selected.Y--; break;
        case 80: if (Game.Selected.Y < 8) Game.Selected.Y++; break;
        }
      Game.DrawCursor();
      }
    // wprowadzenie cyfry
    if ( (Key >= 48) and ( Key <= 57) )
      {
        Game.SetNumber(Game.Selected.X, Game.Selected.Y, Key - 48);
      }

    } while (Key!=27);
  }
