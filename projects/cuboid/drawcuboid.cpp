#include <iostream>
#include <conio2.h>
#include "drawcuboid.h"
using namespace std;

// rysowanie poziomej linni
void DrawHLine(int x, int y, int dl)
  {
  gotoxy(x, y);
  for(int i=x;i<x+dl;i++)
    {
    cout << '_';
    }
  }

// rysowanie pionowej linni
void DrawVLine(int x, int y, int dl)
  {
  for(int i=y;i<y+dl;i++)
    {
    gotoxy(x, i);
    cout << '|';
    }
  }

// rysowanie linni ukosnej
void DrawXLine(int x, int y, int dl)
  {
  for(int i=0;i<dl;i++)
    {
    gotoxy(x+i, y-i);
    cout << '/';
    }
  }

// rysowanie prostokatu
void DrawRectangle(int x, int y, int width, int height)
  {
  DrawHLine(x+1, y, width-1);
  DrawVLine(x, y+1, height-1);
  DrawHLine(x+1, y+height-1, width-1);
  DrawVLine(x+width, y+1, height-1);
  }

// rysowanie calego prostopadloscianu
void DrawCuboid(int x, int y, int width, int height, int deep)
  {
  DrawRectangle(x+deep, y-deep, width, height);
  DrawXLine(x, y, deep);
  DrawXLine(x+width, y, deep);
  DrawXLine(x+1, y+height-2, deep-1);
  DrawXLine(x+width+1, y+height-1, deep);
  DrawRectangle(x, y, width, height);
  }
