/****************************************************************
  plik pomocny przy rysowaniu prostopadloscianu

  funkcja ponizej sluzy do rysowania prostopadloscianu, ktorej lewy gorny rog
  przedniej sciany znajduje sie w punkcie podanym poprzez parametry x, y.
  Dlugosc i Szerokosc sciany przedniej jest podana w parametrach width i height
  Parametr deep okresla przesuniecie tylnej sciany w gore i w prawo wzgledem
  przedniej sciany

****************************************************************/
#ifndef __drawcuboid_H__
#define __drawcuboid_H__

void DrawCuboid(int x, int y, int width, int height, int deep);

// funkcje pomocnicze
void DrawHLine(int x, int y, int dl); // linia pozioma
void DrawVLine(int x, int y, int dl); // linia pionowa
void DrawXLine(int x, int y, int dl); // linia ukosna
void DrawRectangle(int x, int y, int width, int height); // caly prostokat

#endif
