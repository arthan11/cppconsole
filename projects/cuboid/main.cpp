#include <iostream>
#include <conio2.h>
#include "..\..\main\menu.h"
#include "..\..\main\literki.h"
#include "drawcuboid.h"
using namespace std;

const string VER         = "1.01"; // wersja programu
const string AUTHOR      = "Krzysztof Lemka"; // autor
const string TITLE       = "CUBOID"; // nazwa programu
const char PRECISION     = 3; // ilosc miejsc po przecinku w liczbach

int fnStart();
int fnMenu();
int fnCalcVolume();

//======================== funkcja glowna ======================================
int main()
  {
  fnStart(); // pokazanie widoku startowego
  fnMenu();  // pwejscie do menu z opcjami programu
  }

//======================== funkcja START =======================================
int fnStart()
  {
  cout.setf(ios::fixed); // liczby wyswietlane w notacji "zwyklej"
  cout.precision(PRECISION); // ustawienie precyzji liczb

  int x;
  // czyszczenie tla
  textbackground(BLUE);
  clrscr();
  // wyswietlenie tytulu - duzy napis na srodku
  textcolor(YELLOW);
  WriteBIG(POS_CENTER, POS_CENTER, TITLE, 192, ' ');
  // wyswietlenie wersji - na srodku pod tytulem
  x = (77 - VER.length()) / 2;
  gotoxy(x, 19);
  textcolor(LIGHTCYAN);
  cout << "ver " << VER;
  // wyswietlenie autora - w prawym dolnym rogu
  x = 75 - AUTHOR.length();
  gotoxy(x, 23);
  textcolor(LIGHTGRAY);
  cout << "by " << AUTHOR;

  // lewy prostopadloscian
  textcolor(LIGHTGREEN);
  DrawCuboid(4, 14, 6, 10, 3);
  // prawy dolny prostopadloscian
  textcolor(LIGHTRED);
  DrawCuboid(63, 7, 10, 7, 5);

  getch();
  }

// =================== funkcje z opcji "O PROGRAMIE" ===========================

// funkcja wykorzystywana w funkcji fnAbout
// rysuje w podanym wierszu na srodku spacje zostawiajac z obu stron odstep
void ClrLine(int y, int margines)
  {
  gotoxy(margines, y);
  cout.width(80-((margines-1)*2));
  cout << " ";
  }

// funkcja pokazuje informacje o programie
int fnAbout()
  {
  // czyszczenie ekranu
  textbackground(BLACK);
  clrscr();

  // naglowek
  textcolor(YELLOW);
  gotoxy(31, 7);
  cout << "CUBOID - INFORMACJE";

  // ustawienie kolorow
  textbackground(BLUE);
  textcolor(WHITE);

  int linia = 10; // tekst bedzie wstawiany od podanego wiersza
  int margines = 15; // zotawiajac po obu stronach margines
  for (int i=linia-1;i<linia+5;i++) // w petli (tyle ile wierszy tekstu)
    ClrLine(i, margines); // czyszczenie linni - rysowanie kolorowego prostokata

  // wypisanie tekstu w odpowiednich miejscach na ekranie
  gotoxy(margines+1,linia++);
  cout << "CUBOID (ang. prostopadloscian) - Program sluzacy ";
  gotoxy(margines+1,linia++);
  cout << "do obliczania objetosci prostopadloscianu na ";
  gotoxy(margines+1,linia++);
  cout << "podstawie podanych przez uzytkownika dlugosci ";
  gotoxy(margines+1,linia++);
  cout << "trzech krawedzi tej bryly geometrycznej: a, b i c.";

  getch(); // czekanie na nacisniecie dowolnego klawisza
  }

// ===================== funkcja z opcji "KONIEC" ==============================
// funkcja powodujaca zakonczenie programu
int fnEnd()
  {
  exit(0); // zakoncz program
  }

// ==================== funkcje do rysowania MENU ==============================

// klasa dziedziczaca po TMenu - nadpisuje funkcje AfterDraw dzieki czemu
// w trakcie wyswietlania menu widac narysowane prostopadlosciany
class TCuboidMenu : public TMenu
  {
  public:
  virtual int AfterDraw();
  };

int TCuboidMenu::AfterDraw()
  {
  // lewy prostopadloscian
  textbackground(BLACK);
  textcolor(LIGHTCYAN);
  DrawCuboid(3, 8, 10, 15, 4);
  // prawy gorny prostopadloscian
  textcolor(LIGHTMAGENTA);
  DrawCuboid(57, 5, 10, 5, 3);
  // prawy dolny prostopadloscian
  textcolor(LIGHTBLUE);
  DrawCuboid(60, 16, 15, 7, 4);
  }

// funkcja wyswietlajaca menu
int fnMenu()
   {
   TCuboidMenu Menu; // tworzenie obiektu menu
   // ustawianie kolorow menu
   Menu.TitleColor = YELLOW;
   Menu.TitleBackground = BLUE;
   Menu.ItemColor = WHITE;
   Menu.SelectedColor = WHITE;
   Menu.ItemBackground = BLUE;
   Menu.SelectedBackground = LIGHTBLUE;
   Menu.FrameColor = LIGHTBLUE;
   Menu.FrameBackground = BLUE;
   // w obecnej wersji mozna dodac maksymalnie 6 opcji menu
   Menu.Add("  OBLICZ OBJETOSC  ", fnCalcVolume);
   Menu.Add("O PROGRAMIE", fnAbout);
   Menu.Add("KONIEC", fnEnd);

   Menu.Draw(); // rysowanie menu
   Menu.Go(); // uruchomienie menu
   }

// =============== funkcje do liczenia objetosci ===============================

// stale kolorow
const char CL_TLO      = BLACK;
const char CL_CUBOID   = LIGHTRED;
const char CL_TXT      = LIGHTGRAY;
const char CL_WZOR     = LIGHTMAGENTA;
const char CL_DANE     = WHITE;
const char CL_WYNIK    = YELLOW;

// zmienne przechowujace dlugosci krawedzi
float KrawedzA, KrawedzB, KrawedzC = 0;
// dlugosc ktorej krawedzi ma byc pobierana
char PobieranaLitera = 'a';

// funckja liczaca objetosc prostopadlosciana
float Objetosc(float a, float b, float c)
  {
  return a*b*c;
  }

// funkcja zaznacza wybrana krawedz na inny kolor
void clcZaznacz(char litera, char kolor = YELLOW)
  {
  textcolor(kolor);

  switch (litera)
    {
    case 'a':
      {
      DrawXLine(71, 23, 7);
      gotoxy(75,21);
      cout << 'a';
      break;
      }
    case 'b':
      {  
      DrawVLine(70, 10, 14);
      gotoxy(71,13);
      cout << 'b';
      break;
      }
    case 'c':
      {
      DrawHLine(51, 23, 19);
      gotoxy(60,24);
      cout << 'c';
      break;
      }
    }
  }

void clcOdswiez()
  {
  textbackground(CL_TLO);
  clrscr();
  // rysuje prostopadloscian
  textcolor(CL_CUBOID);
  DrawCuboid(50, 9, 20, 15, 7);
  gotoxy(75,21);
  cout << 'a';
  gotoxy(71,13);
  cout << 'b';
  gotoxy(60,24);
  cout << 'c';

  // zaznacza odpowiednia krawedz na zolto, mozna podac jako parametr inny kolor
  clcZaznacz(PobieranaLitera);
  // wyswietla dlugosci krawedzi
  textcolor(CL_DANE);
  if (KrawedzA>0)
    {
    gotoxy(3, 2);
    cout << "a = " << KrawedzA;
    }
  if (KrawedzB>0)
    {
    gotoxy(3, 3);
    cout << "b = " << KrawedzB;
    }
  if (KrawedzC>0)
    {
    gotoxy(3, 4);
    cout << "c = " << KrawedzC;
    }
  }

// funkcja pobiera liczbe od uzytkownika
float PobierzDlugosc(string komunikat)
  {
  float f;
  char s[256]; // zmienna pomocnicza

  do // w petli ...
    {
    clcOdswiez(); // odswieza ekran
    textcolor(CL_TXT); // ustawia kolor tekstu
    gotoxy(1, 10);
    cout << komunikat; // wyswietla komunikat
    cin >> s;  // pobiera znaki wprowadzone przez uzytkownika
    f = atof(s); // probuje zamienic znaki na liczbe
    // w razie wystapienia bledow wyswietlenie odpowiedniego komunikatu
    if (f==0)
      {
      cout << "zla liczba!";
      getch();
      }
    else
      if (f<0)
        {
        cout << "liczba musi byc wieksza od zera!";
        getch();
        }
    } while (f<=0); // tak dlugo az liczba bedzie poprawna

  return f;
  }

// funkcja wyswietla wynik po obliczeniu objetosci
void  clcPokazWynik()
  {
  // wyswietla wzor
  gotoxy(40, 5);
  textcolor(CL_WZOR);
  cout << "V = abc";

  // wyswietla wynik
  gotoxy(3, 14);
  textcolor(CL_TXT);
  cout << "Objetosc prostopadloscianu wynosi ";

  gotoxy(3, 16);
  textcolor(CL_WYNIK);
  cout << Objetosc(KrawedzA, KrawedzB, KrawedzC);
  }

// GLOWNA FUNKCJA LICZENIA OBJETOSCI
int fnCalcVolume()
  {
  // zerowanie zmiennych
  KrawedzA = 0;
  KrawedzB = 0;
  KrawedzC = 0;

  PobieranaLitera = 'a';
  KrawedzA = PobierzDlugosc("Podaj dlugosc krawedzi a: ");
  PobieranaLitera = 'b';
  KrawedzB = PobierzDlugosc("Podaj dlugosc krawedzi b: ");
  PobieranaLitera = 'c';
  KrawedzC = PobierzDlugosc("Podaj dlugosc krawedzi c: ");
  PobieranaLitera = 'x';
  clcOdswiez();
  clcPokazWynik();

  getch(); // czeka na wcisniecie klawisza
  }
